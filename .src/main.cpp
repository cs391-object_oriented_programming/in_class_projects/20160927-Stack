#include <iostream>
#include "stack.cpp"
using namespace std;

int main() {
	Stack<int> a_stack; //Note the template argument
	a_stack.init();
	a_stack.push(4);
	a_stack.push(3);
	a_stack.push(444);
	a_stack.push(555);
	a_stack.push(333333);
	a_stack.push(666);	
	a_stack.pop();
	a_stack.dump_stack();
	
	Stack<char> b_stack; //And here
	b_stack.init();
	b_stack.push('g');
	b_stack.dump_stack();
	
	Stack<string> c_stack;
	c_stack.init();
	c_stack.push("Hello World");
	c_stack.push("On top of it");
	c_stack.dump_stack();
	
	return 0;
}
