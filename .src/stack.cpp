#include <iostream>
using namespace std;

template <class Type> // class template
class Stack {
	public:
		enum {MaxStack = 5};
		void init() {
			top = -1;
		}
		void push( Type n ) { // Notice the parameter Type
			if ( isFull() ) {
				cerr << "Full Stack. DON'T PUSH\n";
				return;
			} else {
				arr[ ++top ] = n;
				return;
			}
		}
		int pop() {
			if (isEmpty() ) {
				cerr << "\tEmpty Stack. Don't Pop\n\n";
				return 1;
			} else
				return arr[top--];
		}
		bool isEmpty() {
			return top < 0 ? top : -1;
		}
		bool isFull() {
			return top >= MaxStack -1 ? top : 0;
		}
		void dump_stack() {
			cout << "The Stack contents, from top to bottom, from a stack dump are: " << endl;
			for (int i = top; i >= 0; i--)
				cout << "\t\t" << arr[i] << endl;
		}
	private:
		int top;
		Type arr[MaxStack]; // class Type
};
